Hello there; welcome 👋🏾

AltSchool Badge Website Badge Linkedin Badge Twitter Badge

I'm a Software Engineering Stundent in Altschool, aspiring Data Analyst as well. I'm passionate about sharing knowledge, documentation and building useful open-source projects, speak at some meetups/conferences...

Here's a quick summary about me:

😊 Pronouns: She/her

💡 Fun fact: I'm currently studying at AltSchool Africa School of Software Engineering Class of 2022.

🌱 I’m currently learning Data Analysis, Python and Cloud Engineering.

😊 I’m looking for help with open source projects, hackathons, internships, and entry-level opportunities.

💼 Job interests: Software Engineer, Data Analyst (Intern or Junior level).

📫 You can view my resume and contact me by emailing ogukhlaudyn@gmail.com

